import React, { Component } from "react";
import { connect } from "react-redux";
import { GridLoader } from "react-spinners";

import Weather from "../../components/Weather";
import Forecast from "../../components/Forecast";
import { getCurrentWeather, getCurrentForecast } from "../../store/actions";
import { Weather as WeatherType } from "../../types/weather";
import { SpinnerItem } from "./styles";
import { Forecast as ForecastType } from "../../types/forecast";

type Props = {
  /** displays the current weather info */
  currentWeather: WeatherType;
  /** displays the forecast for the next 3 days */
  forecast: Array<ForecastType>;
  /** true if it's fetching the weather info */
  isWeatherLoading: boolean;
  /** true if it's fetching the forecast info */
  isForecastLoading: boolean;
  /** error message that appears if a problem ocurred on the weather service */
  weatherErrorMsg: string;
  /** error message that appears if a problem ocurred on the forecast service */
  forecastErrorMsg: string;
  /** fires when component mounts and fetches the current weather & info */
  onMount: () => any;
};

/** Climate Component that renders Weather/Forecast and a Spinner when is loading */
class Climate extends Component<Props> {
  componentDidMount() {
    this.props.onMount();
  }

  /** returns true if the forecast and weather service is loading */
  private get isLoading() {
    return this.props.isForecastLoading && this.props.isWeatherLoading;
  }

  /** returns an error message if data couldn't be loaded */
  private get errorMsg() {
    return this.props.weatherErrorMsg !== "" ||
      this.props.weatherErrorMsg !== ""
      ? `An unexpected error ocurred please try again :(`
      : "";
  }

  /** @inheritdoc */
  render() {
    const { currentWeather, forecast } = this.props;
    return this.isLoading ? (
      <SpinnerItem>
        <GridLoader size={40} loading={this.isLoading} color={"#fefefe"} />
      </SpinnerItem>
    ) : this.errorMsg !== "" ? (
      <h2 className="text-white text-center m-3"> {this.errorMsg} </h2>
    ) : (
      <div>
        <Weather info={currentWeather} />
        <Forecast data={forecast} />
      </div>
    );
  }
}

export default connect(
  ({ weatherReducer, forecastReducer }: any) => ({
    currentWeather: weatherReducer.currentWeather,
    forecast: forecastReducer.forecast,
    isWeatherLoading: weatherReducer.loading,
    isForecastLoading: forecastReducer.loading,
    weatherErrorMsg: weatherReducer.error,
    forecastErrorMsg: forecastReducer.error
  }),
  (dispatch: any) => ({
    onMount: () => {
      dispatch(getCurrentWeather());
      dispatch(getCurrentForecast());
    }
  })
)(Climate);
