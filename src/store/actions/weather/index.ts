import {
  GET_WEATHER,
  GET_WEATHER_SUCCESS,
  GET_WEATHER_FAILURE
} from "../../constants";
import { getWeather } from "../../../services/weatherService";

export const getWeatherRequest = () => ({
  type: GET_WEATHER
});

export const getWeatherSuccess = (data: any) => ({
  payload: data,
  type: GET_WEATHER_SUCCESS
});

export const getWeatherFailure = (error: string) => ({
  error,
  type: GET_WEATHER_FAILURE
});

export const getCurrentWeather = () => async (dispatch: any) => {
  dispatch(getWeatherRequest());
  try {
    const response = await getWeather();
    dispatch(getWeatherSuccess(response.data));
  } catch (error) {
    dispatch(getWeatherFailure(error.message));
  }
};
