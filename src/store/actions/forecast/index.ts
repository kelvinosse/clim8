import {
  GET_FORECAST,
  GET_FORECAST_SUCCESS,
  GET_FORECAST_FAILURE
} from "../../constants";
import { getForecast } from "../../../services/forecastService";

export const getForecastRequest = () => ({
  type: GET_FORECAST
});

export const getForecastSuccess = (data: any) => ({
  payload: data,
  type: GET_FORECAST_SUCCESS
});

export const getForecastFailure = (error: string) => ({
  error,
  type: GET_FORECAST_FAILURE
});

export const getCurrentForecast = () => async (dispatch: any) => {
  dispatch(getForecastRequest());
  try {
    const response = await getForecast();
    dispatch(getForecastSuccess(response.data));
  } catch (error) {
    dispatch(getForecastFailure(error.message));
  }
};
