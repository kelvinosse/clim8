import {
  GET_FORECAST,
  GET_FORECAST_SUCCESS,
  GET_FORECAST_FAILURE
} from "../constants";
import { Forecast } from "../../types/forecast";

type InitialState = {
  /** Forecast array that shows all the corresponding weather info for the next 2 days */
  forecast: Array<Forecast>;
  /** true when is requesting the forecast info */
  loading: boolean;
  /** populates when an unexpected error has ocurred */
  error: string;
};

const initState: InitialState = {
  forecast: [],
  loading: false,
  error: ""
};

function forecastReducer(state = initState, action: any) {
  switch (action.type) {
    case GET_FORECAST: {
      return {
        ...state,
        loading: true
      };
    }
    case GET_FORECAST_SUCCESS: {
      return {
        ...state,
        forecast: action.payload,
        loading: false
      };
    }
    case GET_FORECAST_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}

export default forecastReducer;
