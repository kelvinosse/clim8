import {
  GET_WEATHER,
  GET_WEATHER_SUCCESS,
  GET_WEATHER_FAILURE
} from "../constants";
import { Weather } from "../../types/weather";

type InitialState = {
  /** populates the current weather info for today */
  currentWeather: Weather;
  /** true when is requesting the weather info */
  loading: boolean;
  /** populates when an unexpected error has ocurred */
  error: string;
};

const initState: InitialState = {
  currentWeather: {
    location: {
      city: "",
      country: ""
    },
    weather: {
      id: "",
      date: "",
      maxTemperature: 0,
      minTemperature: 0,
      precipitationAmount: 0,
      precipitationPercentage: 0,
      temperatureMorning: 0,
      temperatureNight: 0,
      temperatureAfternoon: 0,
      temperatureFeelsLike: 0,
      weatherIcon: "",
      weatherIconAfternoon: "",
      weatherIconMorning: "",
      weatherIconNight: "",
      weatherDescription: "",
      weatherDescriptionAfternoon: "",
      weatherDescriptionMorning: "",
      weatherDescriptionNight: "",
      windSpeed: 0
    }
  },
  loading: false,
  error: ""
};

function weatherReducer(state = initState, action: any) {
  switch (action.type) {
    case GET_WEATHER: {
      return {
        ...state,
        loading: true
      };
    }
    case GET_WEATHER_SUCCESS: {
      return {
        ...state,
        currentWeather: action.payload,
        loading: false
      };
    }
    case GET_WEATHER_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.error
      };
    }
    default:
      return state;
  }
}

export default weatherReducer;
