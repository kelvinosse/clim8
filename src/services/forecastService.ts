import { createRequest } from "./config";

const request = createRequest();

export const getForecast = () => {
  return request.get("/forecast");
};
