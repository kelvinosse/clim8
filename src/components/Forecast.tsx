import React, { PureComponent } from "react";
import { Forecast as ForecastType } from "../types/forecast";

type Props = {
  /** The current data with the forecast list */
  data: Array<ForecastType>;
};

type WeatherCardType = {
  /** displays the current day */
  day: string;
  /** displays the minimum temperature */
  temperatureMin: number;
  /** displays the maximum temperature */
  temperatureMax: number;
  /** displays the current icon */
  icon: string;
  /** displays the current description */
  description: string;
};

/** days of the week */
const days = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday"
];

/** Renders a Weather Column Card that displays the forecast info */
const WeatherCard: React.FC<WeatherCardType> = ({
  day,
  temperatureMin,
  temperatureMax,
  icon,
  description
}: WeatherCardType) => {
  return (
    <div className="col mb-4">
      <div className="card opacity h-100">
        <div className="card-body text-white">
          <h3 className="text-center">{day}</h3>

          <img
            src={`/assets/${icon}`}
            alt="Weather icon"
            className="weather-day-icon"
          />

          <h2 className="text-center mt-3">
            {temperatureMin}&deg; / {temperatureMax}&deg;
          </h2>

          <h5 className="text-center text-capitalize">{description}</h5>
        </div>
      </div>
    </div>
  );
};

/** Renders a Forecast component that transforms a provided data to a visual forecast for the next days */
class Forecast extends PureComponent<Props> {
  render() {
    const { data } = this.props;
    return (
      <div className="row">
        {data.map((forecast, i) => {
          const weatherProps = {
            day: days[new Date(forecast.date).getDay()],
            temperatureMin: forecast.minTemperature,
            temperatureMax: forecast.maxTemperature,
            icon: forecast.weatherIcon,
            description: forecast.weatherDescription
          };
          return <WeatherCard key={i} {...weatherProps} />;
        })}
      </div>
    );
  }
}

export default Forecast;
