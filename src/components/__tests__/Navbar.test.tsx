import React from "react";
import { render } from "@testing-library/react";
import Navbar from "../Navbar";

describe("Navbar Component", () => {
  it("should render Navbar Component with provided logo on it", async () => {
    const { findByAltText } = render(<Navbar />);
    const result = await findByAltText("brand-logo");
    expect(result).toHaveAttribute("src", "assets/logo.svg");
  });
});
