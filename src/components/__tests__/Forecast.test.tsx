import React from "react";
import { render } from "@testing-library/react";
import Forecast from "../Forecast";

describe("Forecast Component", () => {
  it("should render a forecast card showing the specs from a corresponding weather info", async () => {
    const props = {
      data: [
        {
          id: "5dfc9d10be1a3781502512d1",
          date: new Date("2019-12-21T00:00:00.000Z"),
          minTemperature: 7,
          maxTemperature: 10,
          precipitationAmount: 0.4,
          precipitationPercentage: 80,
          windSpeed: 4,
          weatherIcon: "thunderstorm.svg",
          weatherIconMorning: "scattered-clouds.svg",
          weatherIconAfternoon: "rain.svg",
          weatherIconNight: "snow.svg",
          weatherDescription: "Cloudy Showers And Thunderstorms",
          weatherDescriptionMorning: "Overcast",
          weatherDescriptionAfternoon: "Cloudy Light Rain",
          weatherDescriptionNight: "Cloudy Heavy Snow",
          temperatureMorning: 7,
          temperatureAfternoon: 9,
          temperatureNight: 8,
          temperatureFeelsLike: 7
        }
      ]
    };
    const { findByText } = render(<Forecast {...props} />);
    const result = await findByText("Friday");
    expect(result).toHaveClass("text-center");
  });
});
