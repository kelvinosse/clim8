import express from "express";
import weatherRouter from "./routes/weatherRouter";
import forecastRouter from "./routes/forecastRouter";

const api = express();

api.use("/weather", weatherRouter);

api.use("/forecast", forecastRouter);

export default api;
