import weatherSymbols from "./resources/weatherSymbols.json";

/** Returns the weather description from provided list of symbols */
export const getWeatherDescription = (symbol: any): string => {
  const item = weatherSymbols.find(item => {
    return item.symbol === symbol;
  });
  return item ? titleCase(item.description) : "missing";
};

/** Returns the weather icon file from provided list of symbols */
export const getWeatherIcon = (symbol: any): string => {
  const item = weatherSymbols.find(item => {
    return item.symbol === symbol;
  });
  return item ? item.imageFile : "none";
};

/** converts string to title case */
const titleCase = (str: any): string => {
  str = str.toLowerCase().split(" ");
  for (let i = 0; i < str.length; i++) {
    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
  }
  return str.join(" ");
};
