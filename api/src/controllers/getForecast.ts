import { Request, Response } from "express";
import { weeronline } from "../resources/response.json";
import { Forecast } from "../types/forecast";
import { getWeatherDescription, getWeatherIcon } from "../utils";

/** Parses the forecast info from weeronline into custom forecast app format */
export const parseForecast = (forecast: any): Forecast => ({
  id: forecast._id,
  date: new Date(forecast["forecast_date"]),
  maxTemperature: forecast["temperature_max"],
  minTemperature: forecast["temperature_min"],
  precipitationAmount: forecast["precipitation_amount"],
  precipitationPercentage: forecast["precipitation_percentage"],
  temperatureMorning: forecast["temperature_morning"],
  temperatureAfternoon: forecast["temperature_afternoon"],
  temperatureNight: forecast["temperature_night"],
  temperatureFeelsLike: forecast["feels_like_temperature_max"],
  weatherIcon: getWeatherIcon(forecast["weather_symbol"]),
  weatherIconAfternoon: getWeatherIcon(forecast["weather_symbol_afternoon"]),
  weatherIconMorning: getWeatherIcon(forecast["weather_symbol_morning"]),
  weatherIconNight: getWeatherIcon(forecast["weather_symbol_night"]),
  weatherDescription: getWeatherDescription(forecast["weather_symbol"]),
  weatherDescriptionAfternoon: getWeatherDescription(
    forecast["weather_symbol_afternoon"]
  ),
  weatherDescriptionMorning: getWeatherDescription(
    forecast["weather_symbol_morning"]
  ),
  weatherDescriptionNight: getWeatherDescription(
    forecast["weather_symbol_night"]
  ),
  windSpeed: forecast["wind_speed_bft"]
});

/** Sends a forecast for the next 2 days from fake weeronline response */
export function getForecast(req: Request, res: Response) {
  res.send(weeronline.data.slice(1).map(parseForecast));
}
