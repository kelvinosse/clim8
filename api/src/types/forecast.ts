export interface Forecast {
  id: string;
  date: Date;
  minTemperature: number;
  maxTemperature: number;
  precipitationAmount: number;
  precipitationPercentage: number;
  windSpeed: number;
  weatherIcon: string;
  weatherIconMorning: string;
  weatherIconAfternoon: string;
  weatherIconNight: string;
  weatherDescription: string;
  weatherDescriptionMorning: string;
  weatherDescriptionAfternoon: string;
  weatherDescriptionNight: string;
  temperatureMorning: number;
  temperatureAfternoon: number;
  temperatureNight: number;
  temperatureFeelsLike: number;
}
